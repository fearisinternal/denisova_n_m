#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		std::string filename = argv[1];
		cv::Mat inp = imread(filename, CV_LOAD_IMAGE_COLOR);
		Mat res = Mat::zeros(inp.rows * 2, inp.cols * 4, CV_8UC3);
		Mat emp = Mat::zeros(inp.rows, inp.cols, CV_8UC1);
		inp.copyTo(res(Rect(0, 0, inp.cols, inp.rows)));
		
		Mat bgr[3];
		split(inp, bgr);
		std::vector<cv::Mat> res_channels_CV3;
		int from_to[] = { 0,0, 1,1, 2,2 };
		for (int channel = 0; channel < 3; channel++) 
		{
			Mat color[] = { bgr[channel], bgr[channel], bgr[channel] };
			res_channels_CV3.push_back(Mat(inp.rows, inp.cols, CV_8UC3));
			mixChannels(color, 3, &res_channels_CV3[channel], 1, from_to, 3);
			res_channels_CV3[channel].copyTo(res(Rect(inp.cols*(channel+1), 0, inp.cols, inp.rows)));
		}

		std::vector<cv::Mat> res_channels_CV1;
		for (int channel = 0; channel < 3; channel++)
		{
			Mat color[] = { emp, emp, emp };
			color[channel] = bgr[channel];
			res_channels_CV1.push_back(Mat(inp.rows, inp.cols, CV_8UC3));
			mixChannels(color, 3, &res_channels_CV1[channel], 1, from_to, 3);
			res_channels_CV1[channel].copyTo(res(Rect(inp.cols*(channel + 1), inp.rows, inp.cols, inp.rows)));
		}

		imshow("Mat", res);

		imwrite("res.png", res);

		waitKey(0);
	}
	return 0;
}