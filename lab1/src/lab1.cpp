#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char* argv[])
{
	//part I
	//A
	Mat image_A = Mat(150, 255*3, CV_8UC1);
	int colorA = -1;
	for (int icol = 0; icol < image_A.cols; icol++)
	{
		if (icol % 3 == 0)
		{
			colorA++;
		}
		image_A.col(icol).setTo(colorA);
	}
	Mat image_A2;
	Mat out_image_A;
	image_A.convertTo(image_A2, CV_32FC1, (float)1/255);
	pow(image_A2, 2.3, image_A2);
	image_A2.convertTo(out_image_A, CV_8UC1, 255);

	image_A.push_back(out_image_A);
	imshow("image", image_A);
	imwrite("../res_part1.png", image_A);

	std::cout << "Press any button!";
	waitKey(0);
	
	//B
	
	Mat out_image(300, 25 * 30, CV_8UC1);
	int color = -5;
	Mat roi1 = out_image(Rect(0, 0, out_image.cols, out_image.rows / 2));
	Mat roi2 = out_image(Rect(0, 150, out_image.cols, out_image.rows / 2));

	for (int icol = 0; icol < roi1.cols; icol++)
	{
		if (icol % 30 == 0)
		{
			color+=10;
		}
		roi1.col(icol).setTo(color);
	}

	roi1.convertTo(roi2, CV_32FC1, (float)1 / 255);
	Mat roi3 = out_image(Rect(0, 150, out_image.cols, out_image.rows / 2));

	pow(roi2, 2.3, roi2);
	roi2.convertTo(roi3, CV_8UC1, 255);

	imshow("image", out_image);
	imwrite("../res_part2.png", out_image);
	
	waitKey(0);
	return 0;
}