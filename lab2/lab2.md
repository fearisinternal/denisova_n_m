## Лабораторная работа 2. Визуализация искажений jpeg-сжатия

Автор: Денисова Н. М.

url: https://gitlab.com/fearisinternal/denisova_n_m/-/tree/master/lab2

### Задание

Для исходного изображения (сохраненного без потерь) создать jpeg версии с двумя уровнями качества (например, 95 и 65). Вычислить и визуализировать на одной “мозаике” поканальные и яркостные различия.

### Результаты

Изображение качества 95:

![](pic95.jpg)

Изображение качества 65:

![](pic65.jpg)

Различия:

В верхней части: различие между оригинальным изображением и качества 95, 
В нижней части: различие между оригинальным изображением и качества 65.

![](image_result.png)

## Текст программы

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char* argv[])
{
	Mat img_input = imread(argv[1], CV_LOAD_IMAGE_COLOR);

	imwrite("pic95.jpg", img_input, { IMWRITE_JPEG_QUALITY, 95 });
	imwrite("pic65.jpg", img_input, { IMWRITE_JPEG_QUALITY, 65 });

	Mat img_95 = imread("pic95.jpg", CV_LOAD_IMAGE_COLOR);
	Mat img_65 = imread("pic65.jpg", CV_LOAD_IMAGE_COLOR);

	std::vector<Mat> image = {img_input, img_95, img_65};
	std::vector<Mat> image_res = { Mat::zeros(img_input.rows, img_input.cols * 4, CV_8UC3), Mat::zeros(img_input.rows, img_input.cols * 4, CV_8UC3), Mat::zeros(img_input.rows, img_input.cols * 4, CV_8UC3) };

	for (int i = 0; i < image.size(); i++)
	{
		Mat bgr_inp[3];
		split(image[i], bgr_inp);
		Mat inp_b, inp_g, inp_r;
		cvtColor(bgr_inp[0], inp_b, COLOR_GRAY2BGR);
		cvtColor(bgr_inp[1], inp_g, COLOR_GRAY2BGR);
		cvtColor(bgr_inp[2], inp_r, COLOR_GRAY2BGR);
		Mat emp = Mat::zeros(image[i].rows, image[i].cols, CV_8UC1);
		Mat gray;
		Mat bgr[3];
		split(image[i], bgr);
		int from_to[] = { 0,0, 1,1, 2,2 };		
		std::vector<cv::Mat> res_channels_CV1;
		for (int channel = 0; channel < 3; channel++)
		{
			Mat color[] = { emp, emp, emp };
			color[channel] = bgr[channel];
			res_channels_CV1.push_back(Mat(image[i].rows, image[i].cols, CV_8UC3));
			mixChannels(color, 3, &res_channels_CV1[channel], 1, from_to, 3);
			res_channels_CV1[channel].copyTo(image_res[i](Rect(image[i].cols*(channel), 0, image[i].cols, image[i].rows)));
		}
		cvtColor(image[i], gray, COLOR_BGR2GRAY);
		cvtColor(gray, gray, COLOR_GRAY2BGR);
		gray.copyTo(image_res[i](Rect(image[i].rows * 3, 0, image[i].cols, image[i].rows)));
	}
	
	Mat image_result95 = (image_res[1] - image_res[0]) * 30;
	Mat image_result65 = (image_res[2] - image_res[0]) * 30;

	Mat image_result = Mat::zeros(img_input.rows*2, img_input.cols * 4, CV_8UC3);

	image_result95.copyTo(image_result(Rect(0, 0, image_result95.cols, image_result95.rows)));
	image_result65.copyTo(image_result(Rect(0, image_result65.rows, image_result65.cols, image_result65.rows)));


	imshow("image", image_result);
	imwrite("../image_result.png", image_result);

	waitKey(0);
	return 0;
}
```

