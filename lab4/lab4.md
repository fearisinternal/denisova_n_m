## Лабораторная работа 4. Использование частных производных для выделения границ

Автор: Денисова Н. М.

url: https://gitlab.com/fearisinternal/denisova_n_m/-/tree/master/lab4

### Задание

1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата $F_1$.
3. Применить второй линейный фильтр и сделать визуализацию результата $F_2$.
4. Вычислить $R=\sqrt{F_1^2 + F_2^2}$  и сделать визуализацию R.

### Результаты

Рис. 1. Исходное тестовое изображение:
![](lab4.src.png)

Рис. 2. Визуализация результата $F_1$ применения фильтра:
![](lab4.viz_dx.png)

Рис. 3. Визуализация результата $F_2$ применения фильтра:
![](lab4.viz_dy.png)

Рис. 4. Визуализация модуля градиента $R$:
![](lab4.viz_gradmod.png)

## Текст программы

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

void draw(Mat& img, int x, int y, const Scalar color_figure, const Scalar color_ground) {
	int d = 200;
	Rect2i rc = { 0, 0, d, d };
	rc.x += x;
	rc.y += y;
	rectangle(img, rc, color_ground, -1);
	Point center = { rc.x + rc.width / 2, rc.y + rc.height / 2 };
	circle(img, center, rc.width / 3, color_figure, -1);
}


int main(int argc, char* argv[])
{
	int dist = 200;
	Mat img(dist * 2, dist * 3, CV_8UC1);
	draw(img, 0, 0, { 0 }, { 127 });
	draw(img, dist, 0, { 127 }, { 255 });
	draw(img, dist * 2, 0, { 255 }, { 0 });
	draw(img, 0, dist, { 0 }, { 255 });
	draw(img, dist, dist, { 127 }, { 0 });
	draw(img, dist * 2, dist, { 255 }, { 127 });

	imwrite("../lab4.src.png", img);

	//Нахождение границ по Собелю:
	Mat matrix = Mat::zeros(3, 3, CV_8SC1);
	matrix.at<int8_t>(0, 0) = -1;
	matrix.at<int8_t>(0, 1) = -2;
	matrix.at<int8_t>(0, 2) = -1;
	matrix.at<int8_t>(1, 0) = 0;
	matrix.at<int8_t>(1, 1) = 0;
	matrix.at<int8_t>(1, 2) = 0;
	matrix.at<int8_t>(2, 0) = 1;
	matrix.at<int8_t>(2, 1) = 2;
	matrix.at<int8_t>(2, 2) = 1;
	Mat lin1;
	filter2D(img, lin1, CV_32F, matrix, Point(-1, -1), 0, BORDER_REFLECT);
	imwrite("../lab4.viz_dx.png", img);

	Mat matrix_2 = matrix.clone();
	matrix.at<int8_t>(0, 1) = 0;
	matrix.at<int8_t>(0, 2) = 1;
	matrix.at<int8_t>(1, 0) = -2;
	matrix.at<int8_t>(1, 2) = 2;
	matrix.at<int8_t>(2, 0) = -1;
	matrix.at<int8_t>(2, 1) = 0;
	Mat lin2;
	filter2D(img, lin2, CV_32F, matrix_2, Point(-1, -1), 0, BORDER_REFLECT);
	imwrite("../lab4.viz_dy.png", img);

	Mat result;
	pow(lin1, 2, lin1);
	pow(lin2, 2, lin2);
	pow((lin1 + lin2), 0.5, result);
	result.convertTo(result, CV_8U);

	imwrite("../lab4.viz_gradmod.png", result);
	waitKey(0);
	return 0;
}
```

