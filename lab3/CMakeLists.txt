project( lab3 )

if ( NOT DEFINED OpenCV_INCLUDE_DIRS )
  find_package( OpenCV 3.1 REQUIRED )
endif()

set( SOURCES  
  src/lab3.cpp
)

add_executable( lab3 ${HEADERS} ${SOURCES})

set(lab3_lnk ${OpenCV_LIBS} ${OpenCV_INCLUDE_DIRS})

target_link_libraries(lab3 ${lab3_lnk})