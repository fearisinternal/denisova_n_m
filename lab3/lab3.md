## Лабораторная работа 3. Обработка изображения в градациях серого

Автор: Денисова Н. М.

url: https://gitlab.com/fearisinternal/denisova_n_m/-/tree/master/lab3

### Задание

1. Подобрать и зачитать небольшое изображение $S$ в градациях серого.
2. Построить и нарисовать гистограмму $H_s$ распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график $V$ табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить $L$, нарисовать гистограмму $H_L$ преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения $C_i$ и их гистограммы $H_{C_{i}}$).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное $S$ и бинаризованное $B_G$ изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное $S$ и бинаризованное $B_L$ изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации $M$.
9. Сделать визуализацию $K$ бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).

### Результаты

Рис. 1. Исходное полутоновое изображение $S$:

![](lab3.src.png)

Рис. 2. Гистограмма $H_s$ исходного полутонового изображения $S$:

![](lab3.hist.src.png)

Рис. 3. Визуализация функции преобразования $V$:

![](lab3.lut.png)

Рис. 4.1. Таблично пребразованное изображение $L$:

![](lab3.lut.src.png)

Рис. 4.2. Гистограмма $H_L$ таблично-пребразованного изображения $L$:

![](lab3.hist.lut.src.png)

Рис. 5.1. Преобразование $C_1$ CLAHE с параметрами clipLimit = 20, tileGridSize=8x8:

![](lab3.clahe.1.png)

Рис. 5.2. Гистограмма $H_{C_{1}}$:

![](lab3.hist.clahe.1.png)

Рис. 5.3. Преобразование $C_2$ CLAHE с параметрами clipLimit = 30, tileGridSize=16x16:

![](lab3.clahe.2.png)

Рис. 5.4. Гистограмма $H_{C_{2}}$:

![](lab3.hist.clahe.2.png)

Рис. 5.5. Преобразование $C_3$ CLAHE с параметрами clipLimit = 40, tileGridSize=4x4:

![](lab3.clahe.3.png)

Рис. 5.6. Гистограмма $H_{C_{3}}$:

![](lab3.hist.clahe.3.png)

Рис. 6. Изображение $S$ до и $B_G$ после глобальной бинаризации:

![](lab3.bin.global.png)

Рис. 7. Изображение $S$ до и $B_L$ после локальной бинаризации:

![](lab3.bin.local.png)

Рис. 8. До и после морфологической фильтрации $M$:

![](lab3.morph.png)

Рис. 9. Визуализация маски $K$:

![](lab3.mask.png)

## Текст программы

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

Mat Hist(Mat img, int height, int scale)
{
	int max_h = 0;
	int max_i = 0;
	int hist[256]{ 0 };
	for (int irow = 0; irow < img.rows; irow++) {
		for (int icol = 0; icol < img.cols; icol++) {
			hist[img.at<uint8_t>(irow, icol)]++;
			if (max_h < hist[img.at<uint8_t>(irow, icol)])
			{
				max_h = hist[img.at<uint8_t>(irow, icol)];
				max_i = img.at<uint8_t>(irow, icol);
			}
		}
	}
	Mat hist_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	hist_img.setTo(255);
	for (auto i = 0; i < 256; i++) {
		rectangle(hist_img, Point(i * scale, height - int(float(hist[i]) / max_h * height)), Point(i * scale + scale, height), Scalar::all(0), -1);
	}
	return hist_img;
}

Mat BrightnessConversion()
{
	Mat function(1, 256, CV_8UC1);
	int y;
	int step = 0;
	for (auto x = 0; x < 256; x++)
	{
		if (step < 64)
		{
			y = step*4;
		}
		else
		{
			y = (step-64)*4;
		}
		step++;
		if (step == 64) {
			step = 0;
		}
		function.at<uint8_t>(x) = y;
	}
	return function;
}

int main(int argc, char* argv[])
{
	Mat img_input = imread(argv[1], CV_8UC1);

	imwrite("../lab3.src.png", img_input);  //pic 1

	int height = 256;
	int scale = 2;
	Mat original_hist = Hist(img_input, height, scale);
	imwrite("../lab3.hist.src.png", original_hist); //pic 2

	Mat lut = BrightnessConversion();
	Mat graph_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	graph_img.setTo(255);
	for (int i = 0; i < 256 - 1; i++) {
		line(graph_img, Point(i * scale, height - lut.at<uint8_t>(0, i)), Point(i * scale + scale, height - lut.at<uint8_t>(0, i + 1)), Scalar::all(0), 1);
	}
	imwrite("../lab3.lut.png", graph_img); //pic 3
	
	Mat res_lut;
	LUT(img_input, lut, res_lut);
	imwrite("../lab3.lut.src.png", res_lut);//pic 4.1

	Mat res_lut_hist = Hist(res_lut, height, scale);
	imwrite("../lab3.hist.lut.src.png", res_lut_hist); //pic 4.2

	Mat img_clahe_1, img_clahe_2, img_clahe_3;
	createCLAHE(20, Size(8, 8))->apply(img_input, img_clahe_1);
	Mat hist_clahe_1 = Hist(img_clahe_1, height, scale);
	imwrite("../lab3.clahe.1.png", img_clahe_1);//pic 5.1
	imwrite("../lab3.hist.clahe.1.png", hist_clahe_1);//pic 5.2

	createCLAHE(30, Size(16, 16))->apply(img_input, img_clahe_2);
	Mat hist_clahe_2 = Hist(img_clahe_2, height, scale);
	imwrite("../lab3.clahe.2.png", img_clahe_2);//pic 5.3
	imwrite("../lab3.hist.clahe.2.png", hist_clahe_2);//pic 5.4

	createCLAHE(40, Size(4, 4))->apply(img_input, img_clahe_3);
	Mat hist_clahe_3 = Hist(img_clahe_3, height, scale);
	imwrite("../lab3.clahe.3.png", img_clahe_3);//pic 5.5
	imwrite("../lab3.hist.clahe.3.png", hist_clahe_3);//pic 5.6

	Mat global_bin;
	threshold(img_input, global_bin, 5, 250, THRESH_BINARY | THRESH_OTSU);
	Mat result_global_bin = Mat::zeros(img_input.rows, img_input.cols * 2, CV_8UC1);
	img_input.copyTo(result_global_bin(Rect(0, 0, img_input.cols, img_input.rows)));
	global_bin.copyTo(result_global_bin(Rect(img_input.cols, 0, img_input.cols, img_input.rows)));
	imwrite("../lab3.bin.global.png", result_global_bin);//pic 6

	Mat local_bin;
	adaptiveThreshold(img_input, local_bin, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 3, 2);
	Mat result_local_bin = Mat::zeros(img_input.rows, img_input.cols * 2, CV_8UC1);
	img_input.copyTo(result_local_bin(Rect(0, 0, img_input.cols, img_input.rows)));
	local_bin.copyTo(result_local_bin(Rect(img_input.cols, 0, img_input.cols, img_input.rows)));
	imwrite("../lab3.bin.local.png", result_local_bin);//pic 7
	
	Mat morphological;
	Mat kernel = Mat::ones(2, 2, CV_32F);
	morphologyEx(global_bin, morphological, MORPH_CLOSE, kernel);
	Mat result_morphological = Mat::zeros(img_input.rows, img_input.cols * 2, CV_8UC1);
	global_bin.copyTo(result_morphological(Rect(0, 0, img_input.cols, img_input.rows)));
	morphological.copyTo(result_morphological(Rect(img_input.cols, 0, img_input.cols, img_input.rows)));
	imwrite("../lab3.morph.png", result_morphological);//pic 8

	Mat binary_mask;
	double alpha = 0.5; double beta = (1.0 - alpha);
	addWeighted(global_bin, alpha, img_input, beta, 0.0, binary_mask);
	imwrite("../lab3.mask.png", binary_mask);//pic 9

	waitKey(0);
	return 0;
}
```

